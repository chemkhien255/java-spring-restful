package vn.hoidanit.jobhunter.util.error;

public class StorageFileException extends Exception {

  public StorageFileException(String message) {
    super(message);
  }
}
